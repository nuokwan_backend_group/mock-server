const {dispatcher} = require('../core/controller');

const prefix = '/';

const path = '/openapi/lists';

const handler = function (req, res, next) {
  res.send(req.app.routerMap.map());
};

const controller = dispatcher().get(prefix, handler);

module.exports = {
  controller,
  path,
  prefix,
};