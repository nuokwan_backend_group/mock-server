const _ = require('lodash');
/**
 * 分页开始 id
 * @param page
 * @param size
 * @returns {*}
 */
const pageStartId = function (page, size) {
  return page - 1 > 0 ? (page - 1) * size + 1 : page
};

/**
 * mock 分页对象key
 * @param key
 * @param size
 * @returns {string}
 */
const pageKey = function (key, size) {
  return key + "|" + size;
};

/**
 * 当前分页URL
 * @param page
 * @param size
 * @param options #可选项
 * eg : {
 *   domain : "",// 域名
 *   pageNum : "", // 页码key
 *   pageSize : "",// 分页量key
 * }
 * @returns {string}
 */
const pageCurrentUrl = function (page, size = 10, options = {}) {
  let {domain = "", pageKey = "pageNum", pageSize = "pageSize"} = options || {};
  if ("" === domain || !_.isString(domain)) {
    return `${pageKey}=${page}&${pageSize}=${size}`;
  }
  if (domain.match(/\?/)) {
    return `${domain}&${pageKey}=${page}&${pageSize}=${size}`;
  }
  return `${domain}?${pageKey}=${page}&${pageSize}=${size}`;
};

/**
 * 下一个页URL
 * @param page
 * @param size
 * @param options
 * eg : {
 *   domain : "",// 域名
 *   pageNum : "", // 页码key
 *   pageSize : "",// 分页量key
 * }
 * @returns {string}
 */
const pageNextUrl = function (page, size, options) {
  return pageCurrentUrl(page + 1, size, options)
};

/**
 * 上一个页URL
 * @param page #当前页码
 * @param size #分页量
 * @param options
 * eg : {
 *   domain : "",// 域名
 *   pageNum : "", // 页码key
 *   pageSize : "",// 分页量key
 * }
 * @returns {string}
 */
const pagePrevUrl = function (page, size, options) {
  return pageCurrentUrl(page - 1 <= 0 ? page : page - 1, size, options)
};

module.exports = {
  pageKey,
  pageStartId,
  pageCurrentUrl,
  pageNextUrl,
  pagePrevUrl,
};