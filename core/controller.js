const engine = require('express');
let dispatcher = engine.Router;

module.exports = {
  engine,
  dispatcher,
};